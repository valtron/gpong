require(
	[
		'jquery', 'gpong/game', 'gpong/drawer', 'gpong/input'
	],
	function($, Game, Drawer, Input) {
		$(function () {
			var gamebox = $('#game');
			var game = new Game();
			var input = new Input(game, gamebox);
			var drawer = new Drawer(game, gamebox);
		});
	}
);