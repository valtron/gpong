define(['class'], function(Class) {
	var Vec2 = Class.extend({
		init: function (x, y) {
			this.x = x;
			this.y = y;
		},
		project: function (v) {
			// Assumes |v| = 1
			
			var norm = Math.sqrt(this.norm2());
			var cth = this.dot(v) / norm;
			
			return new Vec2(
				v.x * cth,
				v.y * cth
			);
		},
		normalize: function () {
			var norm = Math.sqrt(this.norm2());
			return new Vec2(this.x / norm, this.y / norm);
		},
		norm2: function () {
			return this.dot(this);
		},
		minus: function (v) {
			return new Vec2(this.x - v.x, this.y - v.y);
		},
		plus: function (v) {
			return new Vec2(this.x + v.x, this.y + v.y);
		},
		dot: function (v) {
			return this.x * v.x + this.y * v.y;
		}
	});
	
	return {
		"Vec2": Vec2,
	};
});