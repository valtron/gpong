define(
	[
		'jquery', 'class',
		'easel/display/Stage',
		'easel/display/Graphics',
		'easel/display/Shape',
		'easel/utils/Ticker'
	],
function
	($, Class, Stage, Graphics, Shape, Ticker)
{
	var Drawer = Class.extend({
		init: function (game, gamebox) {
			var self = this;
			
			this.game = game;
			var canvas = $('.field', gamebox)[0];
			canvas.focus();
			this.stage = new Stage(canvas);
			this.state = 1;
			
			$('.pauseplay', gamebox).click(function () {
				if (self.state == 1) {
					self.state = 0;
					$(this).text('Play');
					Ticker.setPaused(true);
				} else {
					self.state = 1;
					$(this).text('Pause');
					canvas.focus();
					Ticker.setPaused(false);
				}
			});
			
			Ticker.addListener(this);
			Ticker.useRAF = true;
			Ticker.setInterval(20);
		},
		tick: function () {
			this.stage.removeAllChildren();
			
			var game = this.game;
			var players = game.players;
			
			for (var i = 0; i < players.length; ++i) {
				var player = players[i];
				
				var g = new Graphics();
				g.beginFill(player.color);
				g.drawCircle(0, 0, player.radius);
				var s = new Shape(g);
				s.x = player.pos.x;
				s.y = player.pos.y;
				this.stage.addChild(s);
			};
			
			{
				var g = new Graphics();
				g.setStrokeStyle(1);
				g.beginStroke(players[0].color);
				var bounds = players[0].bounds;
				g.moveTo(bounds.max.x, bounds.max.y);
				g.lineTo(bounds.max.x, bounds.min.y);
				var s = new Shape(g);
				this.stage.addChild(s);
			}
			
			{
				var g = new Graphics();
				g.setStrokeStyle(1);
				g.beginStroke(players[1].color);
				var bounds = players[1].bounds;
				g.moveTo(bounds.min.x, bounds.max.y);
				g.lineTo(bounds.min.x, bounds.min.y);
				var s = new Shape(g);
				this.stage.addChild(s);
			}
			
			{
				var ball = game.ball;
				
				var g = new Graphics();
				g.beginFill(ball.color);
				g.drawCircle(0, 0, 5);
				var s = new Shape(g);
				s.x = ball.pos.x;
				s.y = ball.pos.y;
				this.stage.addChild(s);
			}
			
			{
				var N = 30;
				
				var preview = this.game.simulate(N);
				
				var g = new Graphics();
				g.setStrokeStyle(1);
				g.beginStroke(Graphics.getHSL(0, 0, 100, 1));
				g.moveTo(preview[0].x, preview[0].y);
				
				for (var i = 1; i < preview.length; ++i) {
					g.lineTo(preview[i].x, preview[i].y);
					g.beginStroke(Graphics.getHSL(0, 0, 100, 1 - i / N));
					g.moveTo(preview[i].x, preview[i].y);
				}
				
				var s = new Shape(g);
				this.stage.addChild(s);
			}
			
			this.stage.update();
			this.game.step();
		}
	});
	
	return Drawer;
});