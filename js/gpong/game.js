define(
	['jquery', 'class', 'gpong/utils', 'easel/display/Graphics'],
function
	($, Class, utils, Graphics)
{
	var G = 1000;
	var FMAX = 10000;
	var VMAX = 30;
	
	function moveBy(pos, vel, bounds, bounciness, callback) {
		var px = pos.x + vel.x;
		var py = pos.y + vel.y;
		
		var xmin = bounds.min.x;
		var xmax = bounds.max.x;
		var ymin = bounds.min.y;
		var ymax = bounds.max.y;
		
		if (px < xmin) {
			px = xmin;
			vel.x *= bounciness;
			if (callback) {
				callback(-1);
			}
		} else if (px > xmax) {
			px = xmax;
			vel.x *= bounciness;
			if (callback) {
				callback(1);
			}
		}
		
		if (py < ymin) {
			py = ymin;
			vel.y *= bounciness;
		} else if (py > ymax) {
			py = ymax;
			vel.y *= bounciness;
		}
		
		pos.x = px;
		pos.y = py;
	}
	
	var Positioned = Class.extend({
		init: function (pos, vel, bounds, bounciness, callback) {
			this.pos = pos;
			this.vel = vel;
			this.bounds = bounds;
			this.bounciness = bounciness;
			this.callback = callback;
		},
		move: function () {
			moveBy(this.pos, this.vel, this.bounds, this.bounciness, this.callback);
		}
	});
	
	var Player = Positioned.extend({
		init: function (pos, bounds, color, scoringArea, radius) {
			this._super(pos, new utils.Vec2(0, 0), bounds, 0);
			this.color = color;
			this.scoringArea = scoringArea;
			this.radius = radius;
		},
	});
	
	function hitCallback(dir) {
		var box = null;
		
		if (dir < 0) {
			box = $('.p2 span');
		} else if (dir > 0) {
			box = $('.p1 span');
		}
		
		if (box) {
			box.text(parseInt(box.text()) + 1);
		}
	}
	
	var Ball = Positioned.extend({
		init: function (pos, vel, bounds, color) {
			this._super(pos, vel, bounds, -0.8, hitCallback);
			this.color = color;
		},
	});
	
	var Bounds = Class.extend({
		init: function (min, max) {
			this.min = min;
			this.max = max;
		}
	});
	
	var Game = Class.extend({
		init: function () {
			var bounds = new Bounds(
				new utils.Vec2(  0,   0),
				new utils.Vec2(640, 480)
			);
			
			var bounds1 = new Bounds(
				new utils.Vec2(  0,   0),
				new utils.Vec2(300, 480)
			);
			
			var bounds2 = new Bounds(
				new utils.Vec2(340,   0),
				new utils.Vec2(640, 480)
			);
			
			this.players = [
				new Player(
					new utils.Vec2(bounds.max.x * 0.2, bounds.max.y / 2),
					bounds1, Graphics.getRGB(255, 255, 0),
					new Bounds(new utils.Vec2(0, 0), new utils.Vec2(20, 480)),
					10
				),
				new Player(
					new utils.Vec2(bounds.max.x * 0.8, bounds.max.y / 2),
					bounds2, Graphics.getRGB(0, 255, 255),
					new Bounds(new utils.Vec2(620, 0), new utils.Vec2(620, 480)),
					10
				)
			];
			
			this.ball = new Ball(
				new utils.Vec2(bounds.max.x / 2, bounds.max.y / 2),
				new utils.Vec2(0, 0), bounds, Graphics.getRGB(0, 255, 0)
			);
		},
		step: function () {
			var ball = this.ball;
			this._updateBallVelocity(ball.pos, ball.vel);
			ball.move();
			
			var players = this.players;
			for (var i = 0; i < players.length; ++i) {
				players[i].move();
			}
		},
		_updateBallVelocity: function (p, v) {
			var players = this.players;
			
			for (var i = 0; i < players.length; ++i) {
				var player = players[i];
				var pos = player.pos;
				
				var dx = pos.x - p.x;
				var dy = pos.y - p.y;
				
				var dx2 = dx * dx;
				var dy2 = dy * dy;
				
				var d = Math.sqrt(dx2 + dy2);
				
				if (d > player.radius) {
					var r2 = dx2 + dy2;
					var r = Math.sqrt(r2);
					var F = G / r2;
					
					if (F > FMAX) {
						F = FMAX;
					}
					
					var dvx = dx / r * F;
					var dvy = dy / r * F;
					
					v.x += dvx;
					v.y += dvy;
					
					var vnorm = Math.sqrt(v.norm2());
					
					if (vnorm > VMAX) {
						var k = VMAX / vnorm;
						v.x *= k;
						v.y *= k;
					}
				} else {
					// Bounce off
					var surface = (new utils.Vec2(-dy, dx)).normalize();
					
					var parr = v.project(surface);
					var perp = v.minus(parr);
					
					v.x = parr.x - perp.x;
					v.y = parr.y - perp.y;
				}
			}
		},
		simulate: function (n) {
			var ret = [];
			
			var ball = this.ball;
			var bounds = ball.bounds;
			var bounciness = ball.bounciness;
			var p = ball.pos;
			var v = new utils.Vec2(ball.vel.x, ball.vel.y);
			
			for (var i = 0; i < n; ++i) {
				var pcopy = new utils.Vec2(p.x, p.y);
				this._updateBallVelocity(pcopy, v);
				moveBy(pcopy, v, bounds, bounciness);
				ret.push(pcopy);
				p = pcopy;
			}
			
			return ret;
		}
	});
	
	return Game;
});