define(
	['jquery', 'class', 'gpong/utils'],
function
	($, Class, utils)
{
	var keymap = {
		37: [1, 'x', -1],
		38: [1, 'y', -1],
		39: [1, 'x',  1],
		40: [1, 'y',  1],
		65: [0, 'x', -1],
		87: [0, 'y', -1],
		68: [0, 'x',  1],
		83: [0, 'y',  1]
	};
	
	var Input = Class.extend({
		init: function (game, gamebox) {
			var self = this;
			
			var c = $('.field', gamebox);
			c.bind('contextmenu', function () { return false; });
			
			var v = 4;
			
			c.keydown(function (event) {
				var cmd = keymap[event.which];
				
				if (cmd != null) {
					var player = game.players[cmd[0]];
					player.vel[cmd[1]] = v * cmd[2];
				}
			});
			
			c.keyup(function (event) {
				var cmd = keymap[event.which];
				
				if (cmd != null) {
					game.players[cmd[0]].vel[cmd[1]] = 0;
				}
			});
		}
	});
	
	return Input;
});